var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors')


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var swaggerRouter = require('./routes/swagger');

var app = express();
const Eureka = require('eureka-js-client').Eureka;

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors())

app.use('/', indexRouter);
app.use('/users2', usersRouter);
app.use('/v2/api-docs', swaggerRouter);


var swaggerUi = require('swagger-ui-express'),
    swaggerDocument = require('./swagger.json');

const appName = 'node-express-app';

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

var getAllUsers = function(req, res, next) {
    const users = [];
    users.push(
      {
        id: 1,
        username: 'admin',
        authorities: ['ROLE_ADMIN', 'ROLE_USER']
      },
      {
        id: 2,
        username: 'juan',
        authorities: ['ROLE_USER']
      },
    );
    res.send(users);
  }
var redirectTo = function(req, res, next) {
    console.log(JSON.stringify(req))
    res.send({message: 'Hello world'});
  }
router = express.Router()
router.route('/' + appName + '/*')
      .get(redirectTo)
router.route('/users')
  .get(getAllUsers);

app.use('/api/v1', router);
app.use('/' + appName, router);
app.use('/', router);


const client = new Eureka({
    // application instance information
    instance: {
      app: appName,
      hostName: '192.168.0.100',
      ipAddr: '192.168.0.100',
      port: {
        '$': 3000,
        '@enabled': true,
      },
      vipAddress: appName,
      dataCenterInfo: {
        name: 'MyOwn',
        '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
      },
      instanceId: appName + ':' + new Date().getTime()
    },
    eureka: {
      // eureka server host / port
      host: 'admin:admin@192.168.0.100',
      port: 8761,
      servicePath: '/eureka/apps/'
    },
  });

  client.start();


module.exports = app;
