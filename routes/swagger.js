var express = require('express');
var router = express.Router();

var swaggerDocument = require('../swagger.json');

/* GET users listing. */
router.get('/', function(req, res, next) {
  if (swaggerDocument) {
      res.send(swaggerDocument);
  } else {
      res.send({});
  }
});

module.exports = router;
