var express = require('express');
var router = express.Router();


/* GET users listing. */
router.get('/', function(req, res, next) {
  const users = [];
  users.push(
    {
      id: 1,
      username: 'admin',
      authorities: ['ROLE_ADMIN', 'ROLE_USER']
    },
    {
      id: 2,
      username: 'juan',
      authorities: ['ROLE_USER']
    },
  );
  res.send(users);
});

module.exports = router;
